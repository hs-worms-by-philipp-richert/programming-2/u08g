#include "cLageSkala.h"

cLageSkala::cLageSkala(double unter_in, double ober_in, double val_in) {
	correctInput(unter_in, ober_in, val_in);	// validate values

	unterGrenze = unter_in;
	oberGrenze = ober_in;
	aktVal = val_in;
}

cLageSkala::cLageSkala(const cLageSkala& cpy) {
	unterGrenze = cpy.unterGrenze;
	oberGrenze = cpy.oberGrenze;
	aktVal = cpy.aktVal;
}

// overwrite output stream
ostream& operator << (ostream& os, cLageSkala& obj) {
	string bar = "------------------------------------------";
	double tmpOber = obj.oberGrenze - obj.unterGrenze;
	double tmpVal = obj.aktVal - obj.unterGrenze;
	double tmpRel = tmpVal / tmpOber;	// relative position of the star
	int tmpPos = (int)round(tmpRel * bar.length());	// absolute position of the star
	
	if (tmpPos < 1) tmpPos = 1;
	else if (tmpPos > bar.length()) tmpPos = bar.length();

	bar[tmpPos-1] = '*';	// insert star

	os << "|" << bar << "|";

	return os;
}

// overwrite input stream
istream& operator >> (istream& is, cLageSkala& obj) {
	double tmpUnter;
	double tmpOber;
	double tmpVal;

	cout << endl << "Untergrenze eingeben: ";
	is >> tmpUnter;

	cout << "Obergrenze eingeben: ";
	is >> tmpOber;

	cout << "Wert eingeben: ";
	is >> tmpVal;

	// validate inputs
	obj.correctInput(tmpUnter, tmpOber, tmpVal);

	obj.unterGrenze = tmpUnter;
	obj.oberGrenze = tmpOber;
	obj.aktVal = tmpVal;

	return is;
}

// overwrite prefix increment operator
cLageSkala& cLageSkala::operator++ () {
	double incStep = (this->oberGrenze - this->unterGrenze) / 42.0;
	aktVal += incStep;

	// prevent overflow
	if (aktVal > oberGrenze) aktVal = oberGrenze;

	return *this;
}

// overwrite postfix increment operator
cLageSkala cLageSkala::operator++ (int) {
	cLageSkala tmpObj = *this;
	++*this;

	return tmpObj;
}

// overwrite prefix decrement operator
cLageSkala& cLageSkala::operator-- () {
	double decStep = (this->oberGrenze - this->unterGrenze) / 42.0;
	aktVal -= decStep;

	// prevent underflow
	if (aktVal < unterGrenze) aktVal = unterGrenze;

	return *this;
}

// overwrite postfix decrement operator
cLageSkala cLageSkala::operator-- (int) {
	cLageSkala tmpObj = *this;
	--*this;

	return tmpObj;
}

void cLageSkala::ausgabe() {
	cout << endl
		<< "Obergrenze: " << oberGrenze << endl
		<< "Untergrenze: " << unterGrenze << endl
		<< "Wert: " << aktVal << endl
		/*<< *this << endl*/;
}

// helper method
// check (and overwrite) values
void cLageSkala::correctInput(double& unter_in, double& ober_in, double& val_in) {
	if (unter_in >= ober_in) {	// unzulaessige Werte
		unter_in = 0.0;
		ober_in = 1.0;
		val_in = 0.5;
		return;
	}

	val_in = (val_in < unter_in) ? unter_in : val_in;	// correct if outside of border
	val_in = (val_in > ober_in) ? ober_in : val_in;
}
#pragma once

#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class cLageSkala
{
private:
	double unterGrenze;
	double oberGrenze;
	double aktVal;
	friend ostream& operator << (ostream&, cLageSkala&);
	friend istream& operator >> (istream&, cLageSkala&);
public:
	cLageSkala(double = 0.0, double = 1.0, double = 0.5);
	cLageSkala(const cLageSkala&);
	cLageSkala& operator ++ ();
	cLageSkala operator ++ (int);
	cLageSkala& operator -- ();
	cLageSkala operator -- (int);
	void ausgabe();
	void correctInput(double&, double&, double&);
};


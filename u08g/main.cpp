#include "cLageSkala.h"

int main() {
	cLageSkala s1;

	s1.ausgabe();

	cout << endl
		<< "Skala:" << endl
		<< s1 << endl;

	++s1;
	++s1;
	++s1;

	cout << endl
		<< "Skala:" << endl
		<< s1 << endl;

	cin >> s1;

	s1.ausgabe();

	cout << endl
		<< "Skala:" << endl
		<< s1 << endl;

	--s1;

	cout << endl
		<< "Skala:" << endl
		<< s1 << endl;

	return 0;
}